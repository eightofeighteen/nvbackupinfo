﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NVBackupInfo
{
    public partial class NVBackupInfp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static string DateCompleted(string Log)
        {
            string line = "";
            
            //Job Message	2014/05/22 23:25:18	47	Jobs	ESVH01	Finished job 47, phase 1 (instance 18) 
            int index = 0;
            List<string> LogList = Log.Split('\n').ToList<string>();
            for (index = LogList.Count - 1; index > 0; index--)
            {
                if (LogList[index].Contains("Finished job"))
                    break;
            }
            //return LogList[index];
            int b = LogList[index].IndexOf("Job Message	") + ("Job Message	").Length;
            /*int e = 0;
            int count = 0;
            int pos = b;
            while (true)
            {
                line += LogList[index][pos];
                pos++;
                if (LogList[index][pos] == ' ')
                    pos++;
                if (count == 2)
                    break;
            }*/
            line = LogList[index].Substring(b, 19);
            DateTime tt = Convert.ToDateTime(line);
            DateTime tto = tt;

            line = tt.TimeOfDay.ToString().Substring(0, tt.TimeOfDay.ToString().Length - 3);
            if (DateTime.Now.Date == tto.Date)
                line = "Today @ " + line;
            else if (DateTime.Now.AddDays(-1).Date == tto.Date)
                line = "Yesterday @ " + line;
            else
                line = tto.Date.ToShortDateString() + " " + line;
            

            return line;
        }

        public static bool IsSlaveJob(NVBackupInfo.NVBackupLine job)
        {
            bool slave = false;
            if (job.Log.Contains("indicates its Master is Job Id"))
                slave = true;
            return slave;
        }
    }
}