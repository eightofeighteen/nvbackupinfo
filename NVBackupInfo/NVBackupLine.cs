﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.Linq;
using System.IO;

namespace NVBackupInfo
{
    using NVBackupLineList = List<NVBackupLine>;
    public static class NVBackupOps
    {
        public static DateTime CleanNow()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        public static NVBackupLineList FilterBackups(NVBackupLineList input)
        {
            NVBackupLineList working = new NVBackupLineList();
            List<string> Names = new List<string>();
            foreach (NVBackupLine line in input)
            {
                if (!Names.Contains(line.Title))
                {
                    Names.Add(line.Title);
                }
            }
            //Console.WriteLine("Names has {0} elements.", Names.Count);
            foreach (string Name in Names)
            {
                int index = -1;
                DateTime max = DateTime.Now.AddYears(-100);
                for (int i = 0; i < input.Count; i++)
                {
                    if (input[i].Title == Name && input[i].Timestamp > max)
                    {
                        max = input[i].Timestamp;
                        index = i;
                    }
                }
                working.Add(input[index]);
            }

            return working;
        }

        public static string GetSingleLog(int kID)
        {
            string log = "";
            NVBackupInfoDBDataContext context = new NVBackupInfoDBDataContext();
            /*var backup =
                    from a in context.GetTable<job>()
                    where (a.Client == client && a.Timestamp > CleanNow())
                    select a;*/

            var backup =
                from a in context.GetTable<job>()
                where (a.kID == kID)
                select a;
            foreach (var elem in backup)
            {
                log = elem.Log;
            }
            return log;
        }

        public static Dictionary<string, NVBackupLineList> GetBackups()
        {
            Dictionary<string, NVBackupLineList> backups = new Dictionary<string, NVBackupLineList>();
            NVBackupInfoDBDataContext context = new NVBackupInfoDBDataContext();
            var clients =
                from a in context.GetTable<v_DistinctClient>()
                select a;
            //Console.WriteLine(clients);
            List<string> ClientList = new List<string>();
            foreach (var client in clients)
            {
                Console.WriteLine(client.Client);
                ClientList.Add(client.Client);
            }
            foreach (string client in ClientList)
            {
                var backup =
                    from a in context.GetTable<job>()
                    where (a.Client == client && a.Timestamp > CleanNow())
                    select a;
                List<NVBackupLine> site = new List<NVBackupLine>();
                foreach (var elem in backup)
                {
                    NVBackupLine ibackup = new NVBackupLine();
                    ibackup.Client = elem.Client;
                    ibackup.Title = elem.Title;
                    ibackup.Status = elem.Status;
                    ibackup.ID = elem.ID;
                    ibackup.Timestamp = elem.Timestamp;
                    ibackup.Log = elem.Log;
                    ibackup.kID = elem.kID;
                    site.Add(ibackup);
                }
                site = FilterBackups(site);
                backups.Add(client, site);
            }
            return backups;
        }
    }

    public class NVBackupLine
    {
        public string ID
        {
            get;
            set;
        }

        public string Client
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string Log
        {
            get;
            set;
        }

        public DateTime Timestamp
        {
            get;
            set;
        }

        public int kID
        {
            get;
            set;
        }

        public NVBackupLine()
        {

        }

        private static bool ListContains(List<string> list, string target, bool lastLine = false)
        {
            if (lastLine)
            {
                if (list[list.Count - 1].Contains(target))
                    return true;
            }
            else
            {

                foreach (string line in list)
                {
                    if (line.Contains(target))
                        return true;
                }
            }
            return false;
        }

        private static void StripBlanks(List<string> list)
        {
            //bool data = false;

            while (list[list.Count - 1].Length == 0)
            {
                list.RemoveAt(list.Count - 1);
            }
        }

        public override string ToString()
        {
            return Client + "\t" + ID + "\t" + Title + "\t" + Timestamp + "\t" + Status + "\t" + (Log.Length * sizeof(char)) + " bytes in log.";
        }

        public XElement generateXElement()
        {


            XElement result = new XElement("NVBackupLine",
                new XElement("Client", Client),
                new XElement("ID", ID),
                new XElement("Title", Title),
                new XElement("Status", Status),
                new XElement("Timestamp", Timestamp),
                new XElement("Log", Log));

            /*XElement result = new XElement("NData",
            new XElement("ComputerName", NisabaCore.ComputerName()),
            new XElement("DomainName", NisabaCore.DomainName()),
            new XElement("FullComputerName", NisabaCore.FullComputerName()),
            new XElement("Polled", DateTime.Now),
            new XElement("HwManufacturer", NisabaCore.HwManufacturer()),
            new XElement("HwModel", NisabaCore.HwModel()),
            new XElement("HwSerialNumber", NisabaCore.HwSerialNumber()),
            new XElement("ComputerUUID", NisabaCore.ComputerUUID()),
            new XElement("DeterminedAssetTag", NisabaCore.DeterminedAssetTag()),
            new XElement("ComputerDistinguishedName", NisabaCore.ComputerDistinguishedName()));

            XElement users = new XElement("Users");

            foreach (UserPair user in UserList)
            {
                users.Add(new XElement("UserPair",
                    new XElement("Username", user.Username),
                    new XElement("LastModified", user.LastModified),
                    new XElement("Active", user.Active)));
            }
            result.Add(users);*/

            return result;
        }

        public string generateXml()
        {
            return generateXElement().ToString();
        }

        public static string Delimit(string info, string delim)
        {
            string result = "";
            int b = info.IndexOf(delim) + delim.Length;
            int e = info.IndexOf("/" + delim);
            result = info.Substring(b + 1, e - b - 2);
            return result;
        }

        public static NVBackupLine readXml(string xml)
        {
            NVBackupLine j = new NVBackupLine();
            j.ID = Delimit(xml, "ID");
            j.Client = Delimit(xml, "Client");
            j.Title = Delimit(xml, "Title");
            j.Status = Delimit(xml, "Status");
            j.Timestamp = Convert.ToDateTime(Delimit(xml, "Timestamp"));
            j.Log = Delimit(xml, "Log");
            return j;
        }

    }
}