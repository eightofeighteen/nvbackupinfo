﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NVBackupLog.aspx.cs" Inherits="NVBackupInfo.NVBackupLog" %>

<script language="C#" runat="server">
    
    

    public static string row(string payload)
    {
        return "<td>" + payload + "</td>";
    }

    public static List<List<string>> FormatLog(string log)
    {
        List<List<string>> FormattedLog = new List<List<string>>();
        //List<string> list = new List<string>();

        List<string> Fulllog = log.Split('\n').ToList<string>();

        foreach (string line in Fulllog)
        {
            List<string> list = line.Split('\t').ToList<string>();
            FormattedLog.Add(list);
        }
        
        
        return FormattedLog;
    }

    public static string DisplayFormattedLog(List<List<string>> FormattedLog)
    {
        string Display = "";
        Display += "<table>";
        foreach (List<string> line in FormattedLog)
        {
            Display += "<tr>";

            for (int i = 0; i < 6; i++)
            {
                Display += "<td>";
                if (i < line.Count)
                    Display += line[i];
                Display += "</td>";
            }

            Display += "</tr>";
        }
        Display += "</table>";
        return Display;
    }
</script>

<%
    
   %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <style>
        body
{
    color:black;
    font-family:Consolas, 'Courier New';
    font-size:8pt;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <%
        /*Dictionary<string, List<NVBackupInfo.NVBackupLine>> backups = NVBackupInfo.NVBackupOps.GetBackups();
        string key = Request.QueryString["kID"];

        string log = "";
        foreach (KeyValuePair<string, List<NVBackupInfo.NVBackupLine>> BackupPair in backups)
        {
            foreach (NVBackupInfo.NVBackupLine singleBackup in BackupPair.Value)
            {
                if (singleBackup.kID.ToString() == key)
                {
                    log = singleBackup.Log;
                }
            }
        }*/
        string key = Request.QueryString["kID"];
        string log = NVBackupInfo.NVBackupOps.GetSingleLog(Convert.ToInt32(key));
        
        //Response.Write(HTMLBreaks(log));

        List<List<string>> FormattedLog = FormatLog(log);
        //DisplayFormattedLog(FormattedLog);
        Response.Write(DisplayFormattedLog(FormattedLog));
         %>
    </div>
    </form>
</body>
</html>
