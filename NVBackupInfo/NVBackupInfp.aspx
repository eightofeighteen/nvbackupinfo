﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NVBackupInfp.aspx.cs" Inherits="NVBackupInfo.NVBackupInfp"%>

<script language="C#" runat="server">
    Dictionary<string, List<NVBackupInfo.NVBackupLine>> backups = NVBackupInfo.NVBackupOps.GetBackups();

    public static string row(string payload, string style = "")
    {
        if (style == "")
            return "<td>" + payload + "</td>";
        else
            return "<td style=\"" + style + "\">" + payload + "</td>";
    }
</script>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Backup Report</title>
    <style>
        table
        /*{
            border-width: 0px;
            padding: 0px;
            border-spacing: 0px;
        }*/
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>Backup List for Today</h1>
        <table>
        <%
            Response.Write("<hr>");
            Response.Write("<tr>");
            Response.Write(row("Server"));
            Response.Write(row("NV ID"));
            Response.Write(row("Title"));
            Response.Write(row("Status"));
            Response.Write(row("Age"));
            Response.Write(row("Actions"));
            Response.Write("</tr>");
            foreach (KeyValuePair<string, List<NVBackupInfo.NVBackupLine>> site in backups)
            {
               
                foreach (NVBackupInfo.NVBackupLine singleBackup in site.Value)
                {
                    if (!IsSlaveJob(singleBackup))
                    {
                        if (singleBackup.Status == "Job aborted" || singleBackup.Status == "Job failed")
                            Response.Write("<tr style=\"background-color: red\">");
                        else if (singleBackup.Status == "In Progress/Other" || singleBackup.Status == "No suitable media for job" || singleBackup.Status == "Waiting for media")
                            Response.Write("<tr style=\"background-color: yellow\">");
                        else
                            Response.Write("<tr>");
                        
                        //Response.Write(singleBackup.ToString());
                        //Response.Write("<td>" + singleBackup.Title + "</td>");
                        Response.Write(row(singleBackup.Client));
                        Response.Write(row(singleBackup.ID));
                        Response.Write(row(singleBackup.Title));
                        if (singleBackup.Status.Contains("Successful"))
                        {
                            Response.Write(row(singleBackup.Status + " (" + DateCompleted(singleBackup.Log) + ")"));
                        }
                        else
                        {
                            Response.Write(row(singleBackup.Status));
                        }

                        Response.Write(row(Math.Round((DateTime.Now - singleBackup.Timestamp).TotalMinutes).ToString(), "text-align: center"));
                        //Response.Write(row(singleBackup.kID.ToString()));
                        string btn = "<a href=\"NVBackupLog.aspx?kID=" + singleBackup.kID.ToString() + "\" title=\"Open log file.\" target=\"_blank\" class=\"btn btn-primary btn-small\">L</a>";
                        Response.Write(row(btn, "text-align: center"));
                        //string LogLink = "<a href=\"NVBackupLog.aspx?kID=" + singleBackup.kID.ToString() + "\" target=\"_blank\">Log</a>";
                        //Response.Write(row(LogLink));
                        
                        Response.Write("</tr>");
                    }
                }
                
            }
          
          %>
        </table>
    </div>
    </form>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
