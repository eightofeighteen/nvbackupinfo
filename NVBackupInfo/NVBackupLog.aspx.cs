﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Management.Instrumentation;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.Linq;

namespace NVBackupInfo
{
    public partial class NVBackupLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static string HTMLBreaks(string input)
        {
            /*string info = input;

            int pos = info.IndexOf('\n');
            while (pos != -1)
            {
                info.Remove(pos, 1);
                info.Insert(pos, "<br>");

                pos = info.IndexOf('\n');
            }

            return info;*/
            return input.Replace("\n", "<br>");
        }
    }
}